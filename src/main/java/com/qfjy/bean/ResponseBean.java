package com.qfjy.bean;


import com.qfjy.enums.ResponseCodeEnum;

//给所有control去用；
public class ResponseBean {
    /**
     * 响应的状态码  比如1001  1002
     */
    private String code;

    /**
     * 响应的描述星系
     */
    private String msg;
    /**
     * 响应的数据 比如list
     */
    private Object obj;
    public ResponseBean(){}

    public ResponseBean(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseBean(String code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj = obj;
    }
    public static ResponseBean failureName(Object obj){
        return new ResponseBean(ResponseCodeEnum.FAILURENAME.getCode(),ResponseCodeEnum.FAILURENAME.getMsg(),obj);
    }

    public static ResponseBean success(){
        return new ResponseBean(ResponseCodeEnum.SUCCESS.getCode(),ResponseCodeEnum.SUCCESS.getMsg());
    }
    public static ResponseBean success(Object obj){
        return new ResponseBean(ResponseCodeEnum.SUCCESS.getCode(),ResponseCodeEnum.SUCCESS.getMsg(),obj);
    }
    public static ResponseBean failure(){
        return new ResponseBean(ResponseCodeEnum.FAILURE.getCode(),ResponseCodeEnum.FAILURE.getMsg());
    }
    public static ResponseBean failure(Object obj){
        return new ResponseBean(ResponseCodeEnum.FAILURE.getCode(),ResponseCodeEnum.FAILURE.getMsg(),obj);
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    @Override
    public String toString() {
        return "ResponseBean{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", obj=" + obj +
                '}';
    }
}

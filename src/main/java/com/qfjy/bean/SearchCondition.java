package com.qfjy.bean;

public class SearchCondition {
    //课程方向名称uid
    private String cname;
    //产品名称
    private String pname;
    //上传者
    private String creater;
    //上传时间
    private String ctime;

    @Override
    public String toString() {
        return "SearchCondition{" +
                "cname='" + cname + '\'' +
                ", pname='" + pname + '\'' +
                ", creater='" + creater + '\'' +
                ", ctime='" + ctime + '\'' +
                '}';
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }
}

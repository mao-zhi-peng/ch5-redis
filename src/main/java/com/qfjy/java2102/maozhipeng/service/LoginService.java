package com.qfjy.java2102.maozhipeng.service;

import ch.qos.logback.core.util.TimeUtil;
import com.qfjy.entity.po.User;
import com.qfjy.entity.po.UserPO;
import com.qfjy.java2102.maozhipeng.exception.PasswordNotRightException;
import com.qfjy.java2102.maozhipeng.exception.UsernameNotRightException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.TimeoutUtils;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @program: ch5-redis
 * @description
 * @author: YouName
 * @create: 2021-08-08 09:06
 **/
@Service
public class LoginService {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     用户在2分钟内，仅允许输入错误密码5次。
     如果超过次数，限制其登录1小时。（要求每登录失败时，都要给相应提式）
     */
    @Resource(name="redisTemplate")
    private ValueOperations<String,String> string;

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Integer> hash;

    /**
     *
     * @param user
     * @return 如果账号正确返回true    否则抛出无账号，密码不对异常
     * @throws UsernameNotRightException
     * @throws PasswordNotRightException
     */
    public boolean checkNamePwd (User user) throws UsernameNotRightException, PasswordNotRightException {
//        if (!addVisitTimes(user.getUsername())){
//            return false;
//        }
        if (redisTemplate.hasKey(user.getUsername())){
            if (string.get(user.getUsername()).equals(user.getPassword())){
                return true;
            }else {
                throw new PasswordNotRightException();
            }
        }else {
            throw  new UsernameNotRightException();
        }
    }


    /**
     * 记录用户登录次数
     * @param username
     * @return
     */
//    用户在2分钟内，仅允许输入错误密码5次
    public Integer addVisitTimes(String username){
        if (!hash.hasKey((username+":count"),"count")){
            hash.put((username+":count"),"count",1);
            redisTemplate.expire((username+":count"),2,TimeUnit.MINUTES);
        }
        hash.increment((username+":count"),"count",1);

        int count  = hash.get((username+":count"),"count");
        if (count>6){
            redisTemplate.expire(username+":count",1, TimeUnit.HOURS);
        }
       return count;
    }




}

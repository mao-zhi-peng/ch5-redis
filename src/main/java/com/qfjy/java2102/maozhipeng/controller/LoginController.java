package com.qfjy.java2102.maozhipeng.controller;

import com.qfjy.bean.ResponseBean;
import com.qfjy.entity.po.User;
import com.qfjy.enums.ResponseCodeEnum;
import com.qfjy.java2102.maozhipeng.exception.PasswordNotRightException;
import com.qfjy.java2102.maozhipeng.exception.UsernameNotRightException;
import com.qfjy.java2102.maozhipeng.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: ch5-redis
 * @description
 * @author: YouName
 * @create: 2021-08-08 09:03
 **/
@Controller
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService loginService;



    @PostMapping("check")
    @ResponseBody
    public ResponseBean checkNamePwd(User user){
        Map<String,String> map
                = new HashMap<>();
        Integer count = loginService.addVisitTimes(user.getUsername());
        if (count>6){
            map.put("obj","已错误5次，一小时限制登录");
            return ResponseBean.failure(map);
        }
        try {

            boolean flag = loginService.checkNamePwd(user);

                return ResponseBean.success();

        } catch (UsernameNotRightException e) {
            map.put("obj","已错误"+count+"次，还剩"+(5-count)+"次机会");
            return ResponseBean.failureName(map);

        } catch (PasswordNotRightException e) {
            map.put("obj","已错误"+count+"次，还剩"+(5-count)+"次机会");
            return ResponseBean.failure(map);
        }


    }
}

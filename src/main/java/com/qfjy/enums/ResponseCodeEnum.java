package com.qfjy.enums;

public enum ResponseCodeEnum {
    SUCCESS("200","请求成功"),
    FAILURE("201","密码错误"),
    FAILURENAME("202","账号错误");

    private String code;
    private String msg;



    ResponseCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

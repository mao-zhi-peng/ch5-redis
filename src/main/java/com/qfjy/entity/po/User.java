package com.qfjy.entity.po;

import lombok.Data;

/**
 * @program: ch5-redis
 * @description
 * @author: YouName
 * @create: 2021-08-08 09:37
 **/
@Data
public class User {
    private String username;
    private String password;
}
